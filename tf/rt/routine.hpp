#pragma once

#include <functional>

namespace tf::rt {

using FiberRoutine = std::function<void()>;

}  // namespace tf::rt
